# Ha SIG

- 在openEuler社区中添加对Ha的支持
- 实施Ha在ARM64平台下的移植、适配和优化工作
- 负责Ha相关软件包的规划、维护和升级
- 处理Ha相关文档的多语言
- 将openEuler社区中Ha的工作成果回馈给上游社区
- 及时响应用户反馈，解决相关问题


# 组织会议

- 公开的会议时间：北京时间，每周二 下午，3点~4点


# 成员

### Maintainers列表
- 侯健[@hjimmy](https://gitee.com/hjimmy)
- 付惠惠[@huihui0311](https://gitee.com/huihui0311)

### Committers列表
- 王迪[@crrs666](https://gitee.com/crrs666)


# 联系方式

- [邮件列表](dev@openeuler.org)
- [IRC频道](#openeuler-dev)
- [IRC公开会议](#openeuler-meeting)


# 项目清单

repository地址：

- https://gitee.com/src-openeuler/pacemaker
- https://gitee.com/src-openeuler/corosync
- https://gitee.com/src-openeuler/fence-agents
- https://gitee.com/src-openeuler/fence-virt
- https://gitee.com/src-openeuler/sbd
- https://gitee.com/src-openeuler/pcs
- https://gitee.com/src-openeuler/corosync-qdevice
- https://gitee.com/src-openeuler/drbd
- https://gitee.com/src-openeuler/drbd-utils
- https://gitee.com/openeuler/ha-api
- https://gitee.com/openeuler/ha-web
- https://gitee.com/openeuler/pacemaker-mgmt

