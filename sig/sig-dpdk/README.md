
# SIG名称
sig-dpdk

申请该SIG组的目的是构建一个DPDK开源软件的开发平台，包括创建仓库，遵循maintainer检视机制等，
该SIG业务范围主要是DPDK PMD驱动的开发维护，暂不需要其他SIG支持


# 组织会议

- 公开的会议时间：北京时间，每周X 下午，XX点~XX点


北京时间，每周五 下午，14点~13点


# 成员

@zhb339 @speech_white

### Maintainer列表

- 郑闳博[@zhb339](https://gitee.com/zhb339)



### Committer列表

- 胡敏[@speech_whie](https://gitee.com/speech_white)



# 联系方式

- [邮件列表](dev@openeuler.org)
- [IRC公开会议](#openeuler-meeting)





# 项目清单

项目名称：dpdk

repository地址：
https://gitee.com/src-openeuler/dpdk.git
- 
- 
