
# Application to create a new SIG
English | [简体中文](./sig-template_cn.md)


Note: The Charter of this SIG follows the convention described in the openEuler charter [README] (/en/governance/README.md), and follows [SIG-governance] (/en/technical-committee/governance/SIG-governance.md).

## SIG Mission and Scope

Describe the Mission and objectives of the new SIG, including but not limited to:
The purpose of applying for the SIG group is to build a DPDK open-source software development platform, including creating a repository and following the maintainer review mechanism,
The SIG service scope is mainly the development and maintenance of the DPDK PMD driver. Other SIGs are not required. 


### Deliverables

What and in what form the SIG is responsible for delivering

- tar

### Repositories and description managed by this SIG
	https://gitee.com/speech_white/dpdk.git

### Cross-domain and external-oriented processes

Cross-domain and externally-oriented processes and actions defined and implemented by this SIG:

- Non-Internal Process Checklist

- The organization guidance plan for the entire openEulerSIG owned by this SIG, etc.


## Basic Information

### Project Introduction
    https://gitee.com/speech_white/dpdk.git

***Tips***: After the SIG is successfully created, https: /gitee.com/openeuler/community/sig/sig-xxxx/ management will be managed by the Maintainer, and the project team can enrich their project introduction, including but not limited to the following content.
```
### Maintainers
- Name (zhb339)

### Committers
- Name (speech_white)

### Mailing list

### IRC Channel

### Conference Information

### External Contact
```


